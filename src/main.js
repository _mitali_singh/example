import React, { useState, useEffect, useRef } from "react";
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  TextInput,
  Image,
  TouchableOpacity,
} from "react-native";
import { tasks } from "./tasks";
import Footer from "./components/footer";
import TaskItem from "./components/taskItem";

const primaryColor = "#00FFCC";
const defaultFont = 18;

const Main = ({}) => {
  const [filter, selectFilter] = useState("");
  const [inAddMode, setInAddMode] = useState(false);
  const [inputText, setInputText] = useState("");
  const [taskData, setTaskData] = useState(tasks);
  const inputRef = useRef();

  useEffect(() => {
    setInputText("");
    inputRef.current.blur();
  }, [inAddMode]);

  const data = filter ? taskData.filter((i) => i.status === filter) : taskData;
  const filteredData =
    !inAddMode && !!inputText
      ? data.filter((item) =>
          item.title.toLowerCase().includes(inputText.toLowerCase())
        )
      : data;

  const onAddItem = () => {
    const tasks = [...taskData];
    tasks.push({
      title: inputText,
      status: "remaining",
    });
    setTaskData([...tasks]);
    setInputText("");
  };

  const onDeleteItem = (index) => {
    const tasks = [...taskData];
    tasks.splice(index, 1);
    setTaskData(tasks);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.headerView}>
        <Text style={styles.headerText}>To do list</Text>
      </View>
      <ScrollView style={styles.main} keyboardShouldPersistTaps={"handled"}>
        <View style={styles.inputView}>
          <TextInput
            ref={inputRef}
            value={inputText}
            onChangeText={(text) => setInputText(text)}
            placeholder={inAddMode ? "Add item" : "Search..."}
            multiline={inAddMode}
            autoCorrect={false}
            autoCapitalize={false}
            placeholderTextColor={"white"}
            style={styles.input}
          />
          {inAddMode && (
            <TouchableOpacity onPress={onAddItem} disabled={!inputText.length}>
              <Image
                source={require("./assets/check.png")}
                style={[styles.image, { opacity: !inputText.length ? 0.3 : 1 }]}
              />
            </TouchableOpacity>
          )}
        </View>
        {filteredData.map((item, index) => (
          <TaskItem data={item} onDelete={() => onDeleteItem(index)} />
        ))}
      </ScrollView>
      <Footer
        data={filteredData}
        filter={filter}
        onSelect={selectFilter}
        inAddMode={inAddMode}
        setInAddMode={setInAddMode}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E5E5E5",
  },
  main: { flex: 1, backgroundColor: "black" },
  headerView: {
    backgroundColor: "#00FFCC",
    paddingHorizontal: 22,
    paddingVertical: 18,
  },
  headerText: {
    fontSize: 18,
    fontWeight: "bold",
    lineHeight: 21.09,
  },
  inputView: {
    flexDirection: "row",
    marginTop: 24,
    marginBottom: 24,
    marginHorizontal: 16,
    paddingRight: 16,
    borderColor: primaryColor,
    borderWidth: 1,
    alignItems: "center",
  },
  input: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    color: "white",
    fontSize: defaultFont,
    flex: 1,
  },
  image: {
    width: 18,
    height: 18,
  },
});

export default Main;
