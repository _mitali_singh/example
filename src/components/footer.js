import React, { useState, Fragment } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";

const selections = [
  {
    id: "",
    label: "All",
  },
  { id: "completed", label: "Completed" },
  {
    id: "remaining",
    label: "Remaining",
  },
];

const Footer = ({ data, filter, onSelect, inAddMode, setInAddMode }) => {
  return (
    <View style={styles.footerView}>
      <TouchableOpacity
        onPress={
          inAddMode ? () => setInAddMode(false) : () => setInAddMode(true)
        }
      >
        <Image
          style={styles.image}
          source={
            inAddMode
              ? require("../assets/cross.png")
              : require("../assets/add.png")
          }
        />
      </TouchableOpacity>
      <Text style={styles.count}>{`${data.length} items`}</Text>
      <View style={styles.selectionButtonView}>
        {selections.map((item, index) => (
          <View>
            <TouchableOpacity
              onPress={() => onSelect(item.id)}
              key={item.id}
              style={[styles.button, index > 0 && { borderLeftWidth: 1 }]}
            >
              <Text>{item.label}</Text>
            </TouchableOpacity>
            {filter === item.id && <View style={styles.indicator} />}
          </View>
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  footerView: {
    paddingHorizontal: 16,
    paddingVertical: 12,
    backgroundColor: "#00FFCC",
    flexDirection: "row",
    alignItems: "center",
  },
  image: {
    height: 18,
    width: 18,
  },
  count: {
    fontSize: 18,
    paddingLeft: 16,
    flex: 1,
  },
  selectionButtonView: {
    flexDirection: "row",
    borderWidth: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    borderRadius: 16,
    paddingHorizontal: 6,
  },
  button: {
    paddingHorizontal: 6,
  },
  indicator: {
    height: 4,
    width: 4,
    borderRadius: 4,
    backgroundColor: "black",
    position: "absolute",
    left: "50%",
    bottom: -10,
  },
});

export default Footer;
