import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";

const TaskItem = ({ data, onDelete }) => {
  const hasCompleted = data.status == "completed";
  return (
    <View style={styles.container}>
      <View style={styles.remaining}>
        {hasCompleted && (
          <Image
            style={styles.image}
            source={require("../assets/blue-check.png")}
          />
        )}
      </View>
      <Text
        style={[
          styles.title,
          hasCompleted && { opacity: 0.7, textDecorationLine: "line-through" },
        ]}
      >
        {data.title}
      </Text>
      <TouchableOpacity onPress={onDelete}>
        <Image style={styles.image} source={require("../assets/delete.png")} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    marginBottom: 16,
    flexDirection: "row",
    alignItems: "center",
  },
  title: {
    fontSize: 16,
    color: "white",
    flex: 1,
    paddingHorizontal: 12,
  },
  remaining: {
    height: 16,
    width: 16,
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "white",
    borderWidth: 1,
  },
  completed: {
    height: 16,
    width: 16,
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  image: { height: 15, width: 15 },
});

export default TaskItem;
